<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">

<meta name="description" content="">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- css -->
<link href="<?php echo URL; ?>assets/css/style.css" rel="stylesheet">
<!-- jQuery -->
<script src="<?php echo URL;?>assets/js/jquery-2.0.3.min.js"></script>



</head>
<body>
	<!-- header -->
	<div id="cssmenu">

<?php
//$url = str_replace('/', '', $_SERVER['REQUEST_URI']);
$url_arr =  explode('/',$_SERVER['REQUEST_URI']);
$url = $url_arr[1];

?>
    <!-- navigation -->

		<ul>
			<!-- same like "home" or "home/index" -->
			<li <?php if ($url == '') echo 'class="active"';?> ><a href="<?php echo URL; ?>">home</a></li>
			<li <?php if ($url == 'mafia') echo 'class="active"';?>><a
				href="<?php echo URL; ?>mafia/index/38070">mafia</a></li>

				
		</ul>

	</div>

	@yield('content')

</body>
</html>