<?php

class HomeModel
{

    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }
    
    public function getDeviceList () 
    {
        $sql = " SELECT d.device_id, d.device_full_name, d.device_name, d.min_value, d.max_value, d.is_active, d.order, "
               . " s.value_boolean, s.value_int, s.value_time, s.value_string, s.timestamp_source, s.timestamp_db "
               . " FROM " . DB_SCHEMA . "device d  "
                 . "  JOIN " . DB_SCHEMA . "asignal s ON s.device_id = d.device_id "
                . " ORDER BY d.device_id ASC ";

        $result = $this->db->ex($sql);
        
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }

   
}
