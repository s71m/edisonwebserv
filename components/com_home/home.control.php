<?php
use Philo\Blade\Blade;
/**
 * Class Home
 *
 * Please note:
 * Don't use the same name for class and method, as this might trigger an (unintended) __construct of the class.
 * This is really weird behaviour, but documented here: http://php.net/manual/en/language.oop5.decon.php
 *
 */

class Home extends Controller
{

    /**
     * PAGE: index
     * This method handles what happens when you move to http://yourproject/home/index (which is the default page btw)
     */
    public function index()
    {/*
        echo $blade->view()->make('home')->render();*/
        require 'templates/header.php';
        $HomeModel = $this->loadModel(__CLASS__, 'HomeModel');
        $deviceList = $HomeModel->getDeviceList();
        require 'components/com_' . strtolower(__CLASS__) . '/' . strtolower(__CLASS__) . '.view.php';
        require 'templates/footer.php';
    }

}
