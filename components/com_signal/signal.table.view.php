<link type="text/css" rel="stylesheet"
	href="<?php echo URL;?>assets/css/theme.blue.css" />
<link type="text/css" rel="stylesheet"
	href="<?php echo URL;?>assets/css/table.css" />
	<script type="text/javascript" 
    src="<?php echo URL;?>assets/js/jquery.tablesorter.min.js"></script>
<script type="text/javascript" 
    src="<?php echo URL;?>assets/js/jquery.tablesorter.widgets.min.js"></script>

<script type="text/javascript"> 


$(function(){
	  $("#table_hst").tablesorter({
		  widgets: ["zebra", "filter", "pager"]
	  });	  
	});
	</script>
<?php  



if (!isset($aSignal)) {
    die("Data does not exist");
} 
?>

<title>Edison / Sensor / <?php echo $aSignal['device_full_name'];?></title>

<h4>Latest Data</h4>



<table id="table_active" class="tablesorter-blue">
<thead>
<tr>
<?php
foreach (array_keys($aSignal) AS $mth) {
    echo '<th>' . $mth . '</th>';
      }
      echo '</tr></thead>';
          
      echo '<tr>';
           
      
      foreach ($aSignal AS $mtd) {
          echo '<td style="text-align:right">';
          if(is_numeric($mtd))
              echo (int ) $mtd;
          else
              echo $mtd;
          echo '</td>';
      
      }
      echo '</tr>';   
?>
   


</table>
<?php 
if (!isset($hstSignal)) {
    die("History Data does not exist");
}
?>
<h4>History Data</h4>
<table id="table_hst" class="tablesorter-blue">
    <thead><tr>
    <?php
    foreach (array_keys($hstSignal[0]) AS $mth) {
        echo '<th>' . $mth . '</th>';
    }
    ?> 
    </tr></thead>

    <?php 
    foreach ($hstSignal AS $mtr) {

    echo '<tr>';
    foreach ($mtr AS $mtd) {
        echo '<td style="text-align:right">';
                if(is_numeric($mtd))
                    echo (int ) $mtd;
                else
                    echo $mtd;
                echo '</td>';

    }
    echo '</tr>';
}
?>
</table>



