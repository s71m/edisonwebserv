<?php

class SignalModel
{

    function __construct($db)
    {
        try {
            $this->db = $db;
        } catch (PDOException $e) {
            exit('Database connection could not be established.');
        }
    }
        
    public function getDeviceList () 
    {
        $sql = " SELECT d.*, CASE	when d.is_active = 0 THEN 'OFF' when d.is_active = 1 THEN 'ON' end is_active_text FROM " . DB_SCHEMA . "device d  "
        . " ORDER BY d.device_id ";

        $result = $this->db->ex($sql);
        
        return $result->fetchAll(PDO::FETCH_ASSOC);
    }
    
    public function getASignalDevice ($p_device_id)
    {
        
        
        $sql = " SELECT d.device_id, d.device_full_name, s.value_boolean, "
           . " s.value_int, s.value_time, s.value_string, s.timestamp_source, s.timestamp_db "

            
		      . " FROM " . DB_SCHEMA . "asignal s  "
              . " JOIN " . DB_SCHEMA . "device d ON s.device_id = d.device_id "   
              . " WHERE s.device_id = " . $p_device_id;
    
        $result = $this->db->ex($sql);
        
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        if ($rows) $row = $rows [0]; else $row = NULL;
        
        return $row;
    }
    
    public function getHstSignalDevice ($p_device_id)
    {
    
    
        $sql = " SELECT d.device_full_name, s.hst_signal_id, s.value_boolean, s.value_int, s.value_time, s.value_string, s.timestamp_source, s.timestamp_db  "
            . " FROM " . DB_SCHEMA . "hst_signal s  "
                . " JOIN " . DB_SCHEMA . "device d ON s.device_id = d.device_id "  
                . " WHERE s.device_id = " . $p_device_id
                . " ORDER BY s.hst_signal_id DESC ";

        $result = $this->db->ex($sql);
    
        $rows = $result->fetchAll(PDO::FETCH_ASSOC);
        if ($rows) return $rows; else return NULL;
    
        
    }
     
}
