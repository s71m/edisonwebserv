
<link type="text/css" rel="stylesheet" href="<?php echo URL;?>assets/css/tabs.css" />

<script src="<?php echo URL;?>assets/js/easyResponsiveTabs.js" type="text/javascript"></script>
 
 <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
 <script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

 
 <?php 

 $catTab='cat'.$url_device_id;

 ?>


<script type="text/javascript">

function changeTab(tabid, device_id){
      $('.resp-tab-item').removeClass('selected');
      $(tabid).addClass('selected');
      var dataString = 'device_id='+ device_id;
      window.history.pushState("object or string", "Title", "<?php echo URL; ?>"+'signal/index/'+ device_id),
      $.ajax({
      type: "POST",
      url: '<?php echo URL; ?>'+'signal/stat/'+ device_id,
      data: dataString,
      cache: false,
      success: function(result){
               $(".resp-tabs-container").html(result);
      }
      });
}
</script>

<!-- Открываем дефолтовый таб -->
<script type="text/javascript">
changeTab('<?php echo $catTab;?>','<?php echo $url_device_id;?>');

</script>

<?php

if (count($deviceList) > 0) {
    ?>
<div id="tabvanilla" class="resp-vtabs"
	style="display: block; width: 100%; margin: 0px;">
	<ul class="resp-tabs-list">
        <?php
    
        //$i_selected = true; // Выделение для первого дефолтового таба
    foreach ($deviceList as $device) {
        if ($device['device_id'] == $url_device_id)
            $selected = "selected";
        else
            $selected = "";
        //$i_selected = false;
        echo '<li class="resp-tab-item ' . $selected . '" id="cat' . $device['device_id'] . '" onclick="changeTab(cat' . $device['device_id'] . ', ' . $device['device_id'] . ')"> '
                   . '<div class="user_container">' . $device['device_name'] . '</div>'
                   . '<div class="user_reply_container">Turn: ' . $device['is_active_text'] . '</div>'
            
        .'</li>';
    }
    ?>
        </ul>
	<!--  Вывод данных-->
	<div class="resp-tabs-container"></div>
</div>


<?php

} else {
    echo 'No Data Found';
}
?>


        
