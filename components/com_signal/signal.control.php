<?php

class Signal extends Controller
{

    public function index($url_device_id)
    {
        $SignalModel = $this->loadModel(__CLASS__, 'SignalModel');
        $deviceList = $SignalModel->getDeviceList();

        require 'templates/header.php';
        require 'components/com_' . strtolower(__CLASS__) . '/' . strtolower(__CLASS__) . '.view.php';
        require 'templates/footer.php';
    }
    
    
    public function stat($p_device_id)
    {   
        if (! isset($p_device_id)) die();
        $SignalModel = $this->loadModel(__CLASS__, 'SignalModel');
        $aSignal = $SignalModel->getASignalDevice($p_device_id);
        $hstSignal = $SignalModel->getHstSignalDevice($p_device_id);
        $deviceId = $p_device_id;
        // load views. within the views we can echo out $songs and $amount_of_songs easily
        require 'components/com_' . strtolower(__CLASS__) . '/' . strtolower(__CLASS__) . '.table.view.php';
        require 'templates/footer.php';
    }

   
}

?>