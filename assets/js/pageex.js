function open(page, timeout, address, onload) {

     timeout = timeout || 5000;
    //browser.set_page_load_timeout(3);
    var page_load_timeout = window.setTimeout(function () {
        page.stop();
        onload('timeout');
        page_load_timeout = false;
    }, timeout);

    page.open(address, function (status) {
        clearTimeout(page_load_timeout);
        if (page_load_timeout !== false) onload(status);
    });
}

module.exports = {
    open: open
}