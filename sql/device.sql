DROP TABLE IF EXISTS `hst_signal`;
DROP TABLE IF EXISTS `signal`;
DROP TABLE IF EXISTS `device`;

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for device
-- ----------------------------

CREATE TABLE `device` (
  `device_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_full_name` varchar(40) DEFAULT NULL,
  `device_name` varchar(40) DEFAULT NULL,
  `min_value` int(11) DEFAULT NULL,
  `max_value` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT 1,
  `order` int(11) DEFAULT NULL,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of device
-- ----------------------------
INSERT INTO `device` VALUES (DEFAULT, 'test device', 'test_dev', '0', '10', DEFAULT, '0');
INSERT INTO `device` VALUES (DEFAULT, 'test dev2', 'dev2', '1', '3', DEFAULT,  '1');

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for signal
-- ----------------------------

CREATE TABLE `signal` (
  `signal_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `value_boolean` tinyint(1),
  `value_int` int(11),
  `value_time` timestamp,
  `value_string` varchar(45),
  `timestamp_source` timestamp,
  `timestamp_db` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`signal_id`),
  UNIQUE KEY `device_id_UNIQUE` (`device_id`),
  CONSTRAINT `fk_device_signal` FOREIGN KEY (`device_id`) REFERENCES `device` (`device_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of signal
-- ----------------------------
INSERT INTO `signal` VALUES (DEFAULT, '1', '1', '5', '2015-11-15 01:38:30', 'testString', DEFAULT, '2015-11-15 01:39:06');

CREATE TABLE `hst_signal` (
  `signal_id` int(11) NOT NULL AUTO_INCREMENT,
  `device_id` int(11) NOT NULL,
  `value_boolean` tinyint(1),
  `value_int` int(11),
  `value_time` timestamp,
  `value_string` varchar(45),
  `timestamp_source` timestamp,
  `timestamp_db` timestamp,
  PRIMARY KEY (`signal_id`),
  CONSTRAINT `fk_device_hstsignal` FOREIGN KEY (`device_id`) REFERENCES `device` (`device_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of signal
-- ----------------------------
INSERT INTO `hst_signal` VALUES (DEFAULT, '1', '1', '5', '2015-11-15 01:38:30', 'testString', DEFAULT, '2015-11-15 01:39:06');

DELIMITER $$

DROP PROCEDURE IF EXISTS `ins_signal` $$ 
CREATE PROCEDURE `ins_signal` (IN p_device_id INT, p_value_boolean INT, p_value_int INT, p_value_time TIMESTAMP, p_value_string varchar(45), p_timestamp_source timestamp)  
BEGIN

  DECLARE var0 INT;

  SELECT COUNT(*) INTO var0	FROM `asignal` where `device_id` = p_device_id;

 IF var0=1 THEN
    INSERT INTO hst_signal (select NULL, a.* from asignal a where a.`device_id` = p_device_id);
		UPDATE asignal SET value_boolean = p_value_boolean, 
											value_int = p_value_int, 
										value_time = p_value_time,
											value_string = p_value_string, 
											timestamp_source = p_timestamp_source, 
											timestamp_db = DEFAULT
		WHERE `device_id` = p_device_id;   
		
	END IF;

  IF var0=0 THEN
    INSERT INTO asignal (signal_id, device_id, value_boolean, value_int, value_time, value_string, timestamp_source, `timestamp_db`) 
				VALUES (DEFAULT, p_device_id, p_value_boolean, p_value_int, p_value_time, p_value_string, p_timestamp_source, DEFAULT) ; 
  

 

  END IF;       
END $$

DELIMITER ;

CALL ins_signal(1, NULL, 5, '2015-11-15 07:25:10', 'stringTest', '2015-11-15 07:00:10');