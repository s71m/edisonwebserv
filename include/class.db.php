<?php

class DB extends PDO
{

    public function __construct($dsn, $username = null, $password = null)
    {
        try {
            // $driver_options[PDO::ATTR_CASE] = PDO::CASE_LOWER;
            // parent::__construct($dsn, $username, $password, $driver_options);
            parent::__construct($dsn, $username, $password);
            $this->exec("SET NAMES 'utf8'");
            $this->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            // $this->setAttribute(PDO::ATTR_EMULATE_PREPARES, false); $this->setAttribute(PDO::ATTR_STRINGIFY_FETCHES, false);
        } catch (PDOException $e) {
            echo "Connection error";
        }
    }

    public function ex($sql, array $params = null)
    {
        $res = null;
        
        $res = $this->prepare($sql);
        // echo $params['timeout'];
        // дополнительная обработка булева значения
        
        $res->execute($params);
        return $res;
    }

    function sql_debug($sql_string, array $params)
    {
        if (! empty($params)) {
            $indexed = $params == array_values($params);
            foreach ($params as $k => $v) {
                if (is_object($v)) {
                    if ($v instanceof \DateTime)
                        $v = $v->format('Y-m-d H:i:s');
                    else
                        continue;
                } elseif (is_string($v))
                    $v = "'$v'";
                elseif ($v === null)
                    $v = 'NULL';
                elseif (is_array($v))
                    $v = implode(',', $v);
                
                if ($indexed)
                    $sql_string = preg_replace('/\?/', $v, $sql_string, 1);
                else
                    $sql_string = str_replace(":$k", $v, $sql_string);
            }
        }
        return $sql_string;
    }
}

?>