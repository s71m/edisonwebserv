<?php

final class Registry
{

    private static $vars;

    public static function get($k, $arr_k = null)
    {
        if ($arr_k !== null) {
            if (isset(self::$vars[$k]) && is_array(self::$vars[$k]) && isset(self::$vars[$k][$arr_k]))
                return self::$vars[$k][$arr_k];
            else
                return null;
        }
        if (isset(self::$vars[$k]))
            return self::$vars[$k];
        else
            return null;
    }

    public static function set($k, $v = null)
    {
        if (is_array($k)) {
            foreach ($k as $name => $value) {
                self::$vars[$name] = $value;
            }
        } else
            self::$vars[$k] = $v;
        return true;
    }

    public static function del($k)
    {
        if (isset(self::$vars[$k])) {
            unset(self::$vars[$k]);
            return true;
        } else
            return false;
    }
}

?>