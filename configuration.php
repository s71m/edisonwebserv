<?php

/**
 * Configuration for: Error reporting
 * Useful to show every little problem during development, but only show hard errors in production
 */
error_reporting(E_ALL);
ini_set("display_errors", 1);
ini_set('date.timezone', 'Etc/GMT-3');
/**
 * Configuration for: Project URL
 * Put your URL here, for local development "127.0.0.1" or "localhost" (plus sub-folder) is fine
 */
define('URL', 'http://edisonwebserv.azurewebsites.net/');


/**
 * Configuration for: Database
 * This is the place where you define your database credentials, database type etc.
*/
define('DB_TYPE', 'mysql');
define('DB_HOST', '94.142.140.117');
define('DB_NAME', 'edison');
define('DB_USER', 'stim');
define('DB_PASS', 'stim');
define('DB_SCHEMA', 'edison.');

define('config_absolute_path', 'C:/www/edisonwebserv.azurewebsites.net');

?>